'use strict';
appAGMS.controller('BusinessInformationCtrl', ['$scope', '$rootScope', '$location', '$route', 'AgmsFactory', function ($scope, $rootScope, $location, $route, AgmsFactory) {
    try {
          currentPage = "BusinessInformation";

          $scope.weblink = "https://www.agms.com";
          $scope.darkcolor = "Dark";
          $scope.lightcolor = "Light";


       var init = function () {

                  if (AgmsFactory.getDeviceConnectivity() != "none") {
                      $scope.InformationData = JSON.parse(localStorage["businessData"]);
                     for(var i=0;i<$scope.InformationData.data.relationships.length;i++){
                       if ($scope.InformationData.data.relationships[i].sfid == sessionStorage["SelectedSFID"]) {
                          $scope.color = $scope.InformationData.data.relationships[i].card_color__c;
                          $scope.logo = $scope.InformationData.data.relationships[i].business_logo__c;
                          $scope.facebook = $scope.InformationData.data.relationships[i].facebook__c;
                          $scope.twitter = $scope.InformationData.data.relationships[i].twitter__c;
                          $scope.mobile = $scope.InformationData.data.relationships[i].sign_in_phone__c;
                          $scope.description = $scope.InformationData.data.relationships[i].description;
                          $scope.street = $scope.InformationData.data.relationships[i].shippingstreet;
                          $scope.city = $scope.InformationData.data.relationships[i].shippingcity;
                          $scope.state = $scope.InformationData.data.relationships[i].shippingstate;
                          $scope.code = $scope.InformationData.data.relationships[i].shippingpostalcode;
                          $scope.textcolor =$scope.InformationData.data.relationships[i].text_color__c;
                          $scope.displayName = $scope.InformationData.data.relationships[i].display_name__c;
                          $scope.website = $scope.InformationData.data.relationships[i].website;
                          $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                          $scope.sfid = $scope.InformationData.data.relationships[i].sfid;
                          $scope.business_username = $scope.InformationData.data.relationships[i].username__c
                          $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;
                          sessionStorage["address"] = JSON.stringify($scope.Address);
                          return;
                        }
                     }
                  } else {
                      AgmsFactory.showToast(NoInterNet, 7000);
                  }
       }
       init();

        $scope.modelChngHide = function () {
            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/AppLayout");
              // $('#modalAccountOrange').hide();

        }


    } catch (e) {
        console.log(e);
    }
}]);
