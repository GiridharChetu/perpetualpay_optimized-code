'use strict';
appAGMS.controller('IndexWhiteCtrl', ['$scope', '$rootScope', '$location', '$route', '$swipe', 'AgmsFactory', function ($scope, $rootScope, $location, $route, swipe,AgmsFactory) {

    try {

        // check api response
        $rootScope.message = "";
        var currentPage = "MainPage";

        //............for calling via Twitter Login   .............//
        $scope.GetTwitterAccount = function () {
           // alert("sucess");
            TwitterConnect.login(
                function (result) {
                    alert('Successful login!');
                    console.log(JSON.stringify(result));
                    //alert(JSON.stringify(result));

                    TwitterConnect.showUser(
                          function (result) {
                          console.log(JSON.stringify(result));
                             var name = result.name.split(' ');
                              var firstName = name[0];
                              var lastName = name[1];
                              //alert(firstName);
                              //alert(lastName);
                          },
                          function (error) {
                             AgmsFactory.showToast(error);
                          }
                    );
                 },
                function (error) {
                    AgmsFactory.showToast(JSON.stringify(error));
                    console.log(error);
                    alert(JSON.stringify("err" +error));
                }
            );



        }
        //............for calling via Twitter Login   .............//


        $scope.GetFacebookAccount = function () {
           var fbLoginSuccess = function (userData) {
                 console.log("UserInfo: ", JSON.stringify(userData));

                facebookConnectPlugin.api( "me/?fields=id,email,name", ["public_profile"],
                    function (response) { alert(JSON.stringify(response)) },
                    function (response) { alert(JSON.stringify(response)) });
          }
                    facebookConnectPlugin.login(["public_profile"], fbLoginSuccess,
                      function loginError (error) {
                        console.error(error)
                      }
              );
        }
        //............for calling via Twitter Login   .............//


        $scope.getSignupAuthKey = function(){

                    var Id = Id_signup;
                    var Pwd = Pwd_signup;
                    var defaultUser = {"username":Id,"password":Pwd};
                    if (AgmsFactory.getDeviceConnectivity() != "none") {
                    $('#loader').show();
                    AgmsFactory.getSignupAuthkey(defaultUser)
                    .success(function (data) {
                           $('#loader').hide();
                        if (!data.error) {
                            refreshToken = data.refresh_token;
                            token  = data.token;
                            localStorage["Token"] = token;
                            $location.path("Main");

                        } else {
                            $('#loader').hide();
                            AgmsFactory.showToast(data.errorMessage, 4000);
                        }
                    })
                    .error(function (data) {
                        $('#loader').hide();
                       if(data.errorMessage=="Bad or expired token."){
                           AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                           $location.path("/Login");
                       }
                       else{
                            AgmsFactory.showToast(data.errorMessage, 4000);

                       }
                    });
                }
                else{
                    AgmsFactory.showToast(NoInterNet, 7000);
                 }
             }

             $scope.Login = function(){
                 $location.path("/Login");
             }
    } catch (e) {
        console.log(e);
    }

 }]);
