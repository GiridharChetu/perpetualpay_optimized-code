appAGMS.controller("AccountsCtrl", ["$scope", "$rootScope", "$location", "$route", "AgmsFactory", function ($scope, $rootScope, $location, $route, AgmsFactory) {
    try {
        $scope.user = {
            pass: "",
            confirmpass: ""
        };
        // $scope.refreshLoader=false;
        currentPage = "Account";
        $scope.checkdevice = device.platform;
        $scope.Android = "Android";
        $scope.iOS = "iOS";
        //............for calling invite contact list .............//
        var init = function () {

            if (AgmsFactory.getDeviceConnectivity() != "none") {
                $scope.settingData = JSON.parse(localStorage["businessData"]);
                sessionStorage["userdata"] = $scope.settingData;
                $scope.users = $scope.settingData;
                $scope.firstname = $scope.settingData.data.firstname;
                $scope.lastname = $scope.settingData.data.lastname;
                $scope.name = $scope.firstname + " " + $scope.lastname;
                $scope.phone = $scope.settingData.data.sign_in_phone__c;
                $rootScope.profile_picture = $scope.settingData.data.profile_picture__c;
                $scope.userName = $scope.settingData.data.username__c;
                $rootScope.email = $scope.settingData.data.email;
                $rootScope.firstName = $scope.settingData.data.firstname;
                $rootScope.lastName = $scope.settingData.data.lastname;
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }

            $scope.showeyeIcon = true;
            $scope.hideeyeIcon = false;
            $("#eyeIcon").hide();

        }
        //.......................for back button......................//
        $scope.business = JSON.parse(localStorage["businessData"]);
        for (var infolength = 0; infolength < $scope.business.data.relationships.length; infolength++) {
            if ($scope.business.data.relationships[infolength].sfid == sessionStorage["SelectedSFID"]) {
                $scope.businesssfid = $scope.business.data.relationships[infolength].sfid;
                break;
            }
        }
        //..................for back button.........................//
        //............for calling invite contact list .............//

        init();

        // check api response
        $rootScope.message = "";
        //............for calling appLayout Page .............//

        $scope.backbtn = function () {
            $rootScope.StopInterval = "false";
            sessionStorage["SelectedSFID"] = $scope.businesssfid;
            $location.path("/AppLayout");
        }
        //............for showing change password Page .............//
        $scope.modelChng = function () {
            $location.path("/ChangePassword");
        }

        // Set the default value of inputType
        $scope.inputType = 'password';

        // Hide & show password function
        $scope.hideShowPassword = function () {
            if ($scope.inputType == 'password') {
                $scope.inputType = 'text';
                $scope.hideeyeIcon = true;
                $scope.showeyeIcon = false;
            } else {
                $scope.inputType = 'password';
                $scope.hideeyeIcon = false;
                $scope.showeyeIcon = true;
            }
        };

        //............for calling change password api .............//
        $scope.user = {};
        $scope.changePwd = function (user) {
            var pass = user.password;
            var confirmpass = user.confirmpassword;

            if (AgmsFactory.getDeviceConnectivity() != "none") {
                if ((pass == "" || pass == undefined) && (confirmpass == "" || confirmpass == undefined) || (pass == "" || pass == undefined) || (confirmpass == "" || confirmpass == undefined)) {
                    $scope.validation($scope.user);
                } else {
                    if (pass == confirmpass) {

                        $('#loader').show();
                        $scope.user.first_name = $rootScope.firstName;
                        $scope.user.last_name = $rootScope.lastName;
                        $scope.user.email = $rootScope.email;
                        $scope.user.picture_src = $rootScope.profile_picture;

                        AgmsFactory
                            .changePassword($scope.user)
                            .success(function (data) {
                                $('#loader').hide();

                                if (!data.error) {
                                    $rootScope.StopInterval = "false";
                                    AgmsFactory.showToast(PwdMsg, 4000);
                                    $location.path("/AppLayout");

                                } else {
                                    $('#loader').hide();
                                    AgmsFactory.showToast(data.errorMessage, 4000);
                                }
                            })
                            .error(function (data) {
                                $('#loader').hide();
                                AgmsFactory.showToast(data);
                                if (data.errorMessage == "Bad or expired token.") {
                                    AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                    $location.path("/Login");
                                } else {
                                    AgmsFactory.showToast(data.errorMessage, 4000);

                                }
                            });

                    } else {
                        AgmsFactory.showToast("Password and Confirm Password should be same", 4000);
                    }
                }
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }
        }

        //...................update user profile....................................//

        $scope.UpdateProfile = function () {
            if (AgmsFactory.getDeviceConnectivity() != "none") {
                if (device.platform == "Android" || device.platform == "android") {
                    var Imageurl = $scope.profile_picture
                    var filesSelected = document.getElementById("inputFileToLoad").files;
                    //var filesSelected = $('#inputFileToLoad')[0].files[0];

                    // check for file size
                    if (filesSelected[0].size > 3145728) {
                        AgmsFactory.showToast('You have selected too big file, please select a one smaller image file', 4000);
                        return;
                    }
                    var ext = $('#inputFileToLoad').val().split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['png', 'jpg', 'jpeg']) == -1) {
                        AgmsFactory.showToast("Please upload files having extension:\n png, jpg, jpeg\n only", 4000);
                        return;
                    }
                    if (filesSelected.length > 0) {
                        var fileToLoad = filesSelected[0];
                        var fileReader = new FileReader();
                        $('#loader').show();
                        fileReader.onload = function (fileLoadedEvent) {
                            var srcData = fileLoadedEvent.target.result; // <--- data: base64
                            srcData = srcData.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                            $scope.Profile_url = {
                                "image": srcData
                            };
                            AgmsFactory.updateProfile($scope.Profile_url)
                                .success(function (data) {
                                    $('#loader').hide();
                                    if (!data.error) {
                                        $rootScope.StopInterval = "false";
                                        $rootScope.profile_picture = data.data.link;
                                        var updatepicname = $rootScope.profile_picture;
                                        $("#updatePicname").val(updatepicname);
                                        var split_image = $rootScope.profile_picture.split('/');

                                        if (split_image[0] == "http:") {

                                            $rootScope.profile_picture = $rootScope.profile_picture.replace("http", "https");
                                        }

                                    } else {
                                        $('#loader').hide();
                                        AgmsFactory.showToast(JSON.stringify(data.error), 4000);
                                    }
                                })
                                .error(function (data) {
                                    $('#loader').hide();
                                    AgmsFactory.showToast("err " + JSON.stringify(data), 4000);
                                    if (data.errorMessage == "Bad or expired token.") {
                                        AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                        $location.path("/Login");
                                    } else {
                                        AgmsFactory.showToast(data.errorMessage, 4000);

                                    }
                                });
                        }
                        fileReader.readAsDataURL(fileToLoad);
                    }

                } else {

                    navigator.camera.getPicture(
                        function (uri) {
                            //alert(uri);
                            var URLsplit = uri.substr(uri.lastIndexOf('/') + 1);

                            $("#inputfile").val(URLsplit);
                            $scope.uploadFile(uri);

                        },
                        function (e) {
                            AgmsFactory.showToast("Profile pic has not been uploaded.");
                        }, {
                            quality: 100,
                            targetWidth: 190,
                            targetHeight: 190,
                            destinationType: navigator.camera.DestinationType.FILE_URI,
                            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
                        });
                }
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }

        }




        $scope.uploadFile = function (fileURL) {
            var win = function (r) {
                $('#loader').hide();

                $rootScope.profile_picture = JSON.parse(r.response).data.link;
                var updatepicname = $rootScope.profile_picture;
                $("#inputfile").val(updatepicname);
                //                rootScope.fields = {
                //                   inputModel: ''
                //                 }

                var split_image = $rootScope.profile_picture.split('/');

                if (split_image[0] == "http:") {
                    $rootScope.profile_picture = $rootScope.profile_picture.replace("http", "https");
                }

                console.log("Code = " + r.responseCode);


                //alert($rootScope.profile_picture);

                console.log("Response = " + JSON.parse(r.response));
                console.log("Sent = " + r.bytesSent);
            }

            var fail = function (error) {
                AgmsFactory.showToast("An error has occurred: Code = " + error.code);
                console.log("upload error source " + error.source);
                console.log("upload error target " + error.target);
            }

            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
            options.mimeType = "image/jpeg";

            var params = {};
            options.params = params;

            var headers = {
                'Authorization': 'Client-ID 072dcaa8ee9f00a',
                'Content-Type': 'application/json'
            };

            options.headers = headers;


            var ft = new FileTransfer();
            $('#loader').show();
            ft.upload(fileURL, encodeURI("https://api.imgur.com/3/upload"), win, fail, options);

        }


        //............for calling update api for update user information.............//
        $scope.updateUserInfo = function () {
            if (AgmsFactory.getDeviceConnectivity() != "none") {
                $('#loader').show();
                var fullname = $scope.name;
                var res = fullname.split(" ");
                var firstname = res[0];
                var lastname = res[1];
                var Email = $scope.email;
                var profileurl = $rootScope.profile_picture;
                $scope.user = {
                    "first_name": firstname,
                    "last_name": lastname,
                    "email": Email,
                    "picture_src": profileurl
                };
                AgmsFactory.changePassword($scope.user)
                    .success(function (data) {
                        $('#loader').hide();

                        if (!data.error) {
                            $rootScope.StopInterval = "false";
                            AgmsFactory.showToast(user_information, 4000);
                            $scope.Screens();
                            // $location.path("/AppLayout");
                        } else {
                            $('#loader').hide();
                            AgmsFactory.showToast(data.errorMessage, 7000);
                        }
                    })
                    .error(function (data) {
                        $('#loader').hide();
                        if (data.errorMessage == "Bad or expired token.") {
                            AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                            $location.path("/Login");
                        } else {
                            AgmsFactory.showToast(data.errorMessage, 4000);

                        }
                    });
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }
        }



        //............for calling business layout page .............//
        $scope.Screens = function () {
            if (AgmsFactory.getDeviceConnectivity() != "none") {
                $("#loader").show();
                AgmsFactory.getUser().success(function ($data) {
                    localStorage["businessData"] = JSON.stringify($data);
                    $('#loader').hide();
                    $rootScope.StopInterval = "false";
                    $location.path("/AppLayout");

                }).error(function (data) {
                    $('#loader').hide();
                    AgmsFactory.showToast("error:" + data);
                    if (data.errorMessage == "Bad or expired token.") {
                        AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                        $location.path("/Login");
                    }
                    $scope.error = "An Error has occured while Loading users! " + data.ExceptionMessage;
                });
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }
        };
        //............for calling business layout page .............//

        //...........................validation..................................//

        $scope.validation = function (user) {
            if (user != undefined) {
                var password = user.password;
                var confirmpassword = user.confirmpassword;
                if ((password == "" || password == undefined) && (confirmpassword == "" || confirmpassword == undefined)) {
                    $scope.showpass = true;
                    $scope.passwordMess = NewPassword;
                    $scope.showconfirmpass = true;
                    $scope.confirmpwdMess = confirmPassword;
                    return false;
                } else if ((password == "" || password == undefined)) {
                    $scope.showpass = true;
                    $scope.passwordMess = NewPassword;
                    return false;
                } else if ((confirmpassword == "" || confirmpassword == undefined)) {
                    $scope.showconfirmpass = true;
                    $scope.confirmpwdMess = confirmPassword;
                    return false;
                } else {
                    return false;
                }

            } else {
                $scope.showpass = true;
                $scope.passwordMess = NewPassword;
                $scope.showconfirmpass = true;
                $scope.confirmpwdMess = confirmPassword;
                return false;
                return false;
            }
        }

        $scope.click_pass = function () {
            $scope.showpass = false;
            $("#eyeIcon").show();
        }
        $scope.click_confirmpass = function () {
            $scope.showconfirmpass = false;
        }

        $scope.logOut = function () {
            localStorage["Token"] = "";
            sessionStorage["SelectedSFID"] = "";
            localStorage["businessData"] = "";
            $location.path("/Login");
        }

        $("#name").keypress(function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) { //Enter keycode
                $scope.updateUserInfo();
                document.activeElement.blur();
            }
        });
        $("#demolabel").keypress(function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) { //Enter keycode
                $scope.updateUserInfo();
                document.activeElement.blur();
            }
        });


        //............for calling update api for update user information.............//
    } catch (e) {
        console.log(ew);
    }
                                    }]);
