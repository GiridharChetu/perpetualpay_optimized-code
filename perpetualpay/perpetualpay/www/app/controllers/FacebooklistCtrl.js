'use strict';
appAGMS.controller('FacebooklistCtrl', ['$scope', '$rootScope', '$location', '$route', 'AgmsFactory', function ($scope, $rootScope, $location, $route, AgmsFactory) {

    try {

        $scope.FBfriendName = [];
        var businessArry = 0;
        currentPage = "Facebooklist";
         var init =function(){
          getbusinessInfo();

          document.addEventListener("deviceready", onDeviceReady, false);

          function onDeviceReady() {
             Getfacebooklist();
          }
       }
        init();
       function getbusinessInfo(){

           $scope.business = JSON.parse(localStorage["businessData"]);
            for (var list=0; list < $scope.business.data.relationships.length; list++) {
               if ($scope.business.data.relationships[list].sfid == sessionStorage["SelectedSFID"]) {
                   $scope.businesslogo = $scope.business.data.relationships[list].business_logo__c;
                   $scope.displayname = $scope.business.data.relationships[list].display_name__c;
                   $scope.reward = $scope.business.data.relationships[list].rewards;
                   break;
               }
           }
       }
       //.................get facebook friends list....................//
       function Getfacebooklist(){

            var fbLoginSuccess = function (userData) {
              console.log("UserInfo: ", JSON.stringify(userData));

               facebookConnectPlugin.api('/me/taggable_friends?pretty=0&limit=300',['email', 'public_profile', 'user_friends'],
                                        function (response) {
                                          var Fbresult = response.data;
                                          for(var i=0;i<Fbresult.length;i++){
                                             $scope.FBfriendName.push(Fbresult[i]);

                                          }
                                          $scope.$apply();
                                      },
                                      function(error){
                                      console.log(error);});
            }

            facebookConnectPlugin.login(["public_profile","email","user_friends"], fbLoginSuccess,
              function loginError (error) {
                console.error(error)
              }
            );
       }
    //.................get facebook friends list....................//
    //.................invite facebook friends list....................//
       $scope.GetFbInvite = function(){
            $scope.phonedialer_popup = false;
            $scope.phoneNumber = null;
             if (AgmsFactory.getDeviceConnectivity() != "none") {
                    $('#loader').show();
                    $scope.AllData = JSON.parse(localStorage["businessData"]);
                    $scope.referid = $scope.AllData.data.sfid;
                    for (businessArry; businessArry < $scope.AllData.data.relationships.length; businessArry++) {
                        if ($scope.AllData.data.relationships[businessArry].sfid == sessionStorage["SelectedSFID"]){
                        $scope.businessid = $scope.AllData.data.relationships[businessArry].sfid;
                         break;
                         }
                    }
                    $scope.inviteBody = {
                        "referred_by_id": $scope.referid,
                        "phone_number": $scope.phoneNumber,
                        "business_id": $scope.businessid
                    }
                    AgmsFactory.getInvite($scope.inviteBody)
                        .success(function ($data) {
                            $('#loader').hide();
                            AgmsFactory.showToast(JSON.stringify($data.errorMessage));
                        }).error(function (data) {
                            $('#loader').hide();
                            if(data.errorMessage=="Bad or expired token."){
                                AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                $location.path("/Login");
                            }
                            else{
                                 AgmsFactory.showToast(data.errorMessage, 4000);

                            }
                        });
                } else {
                    AgmsFactory.showToast(NoInterNet, 7000);
                }
       }
     //.................invite facebook friends list....................//


        // check api response
        $rootScope.message = "";
        $scope.Login = function () {
            $location.path("/Login");
        }
        //............for back button  .............//
        $scope.fbbackbtn = function () {
         $rootScope.StopInterval = "false";
            $location.path("/AppLayout");
        }
        $scope.showfacebookdialer = function(){
            $scope.showphonedialer = true;
        }
    }
    catch (e) {
        console.log(e);
    }

}]);
