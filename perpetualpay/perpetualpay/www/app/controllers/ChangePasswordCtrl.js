appAGMS.controller("ChangePasswordCtrl", ["$scope", "$rootScope", "$location", "$route", "AgmsFactory", function ($scope, $rootScope, $location, $route, AgmsFactory) {
    try {
        currentPage = "changePassword";
        $scope.showeyeIcon = true;
        $scope.hideeyeIcon = false;
          $("#eyeIcon").hide();

        //.......................for back button......................//
             $scope.business = JSON.parse(localStorage["businessData"]);
             for (var infolength=0; infolength < $scope.business.data.relationships.length; infolength++) {
                 if ($scope.business.data.relationships[infolength].sfid == sessionStorage["SelectedSFID"]) {
                     $scope.businesssfid = $scope.business.data.relationships[infolength].sfid;
                        break;
                 }
             }
        //..................for back button.........................//

        //............for hide change password Page .............//
        $scope.modelChngHide = function () {
        sessionStorage["SelectedSFID"] = $scope.businesssfid;
            $location.path("/Accounts");

        }
        // Set the default value of inputType
          $scope.inputType = 'password';

            // Hide & show password function
            $scope.hideShowPassword = function(){
              if ($scope.inputType == 'password'){
              $("#passwordlbl").addClass("active");
                $scope.inputType = 'text';
                $scope.hideeyeIcon = true;
                $scope.showeyeIcon = false;
                }
              else{
                $scope.inputType = 'password';
                $scope.hideeyeIcon = false;
                $scope.showeyeIcon = true;
                $("#passwordlbl").addClass("active");
                }
            };

        //............for calling change password api .............//
        $scope.user = {};
        $scope.changePwd = function (user) {
        var pass = user.password;
        var confirmpass = user.confirmpassword;

            if (AgmsFactory.getDeviceConnectivity() != "none") {
                 if((pass == "" || pass == undefined) && (confirmpass == "" || confirmpass == undefined) || (pass == "" || pass == undefined) || (confirmpass == "" || confirmpass == undefined)){
                    $scope.validation($scope.user);
                 }
                 else{
                        if(pass == confirmpass){

                            $('#loader').show();
                            $scope.user.first_name = $rootScope.firstName;
                            $scope.user.last_name = $rootScope.lastName;
                            $scope.user.email = $rootScope.email;
                            $scope.user.picture_src = $rootScope.profile_picture;

                            AgmsFactory
                                .changePassword($scope.user)
                                .success(function (data) {
                                    $('#loader').hide();

                                    if (!data.error) {
                                        $rootScope.StopInterval = "false";
                                        AgmsFactory.showToast(PwdMsg, 4000);
                                        $location.path("/AppLayout");

                                    } else {
                                        $('#loader').hide();
                                        AgmsFactory.showToast(data.errorMessage, 4000);
                                    }
                                })
                                .error(function (data) {
                                    $('#loader').hide();
                                    if(data.errorMessage=="Bad or expired token."){
                                        AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                        $location.path("/Login");
                                    }
                                    else{
                                         AgmsFactory.showToast(data.errorMessage, 4000);

                                    }

                                });

                       }
                        else{
                         AgmsFactory.showToast("Password and Confirm Password should be same",4000);
                         }
                 }
            }
            else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }
        }

         //...........................validation..................................//

                $scope.validation = function(user){
                    if (user != undefined) {
                        var password = user.password;
                        var confirmpassword = user.confirmpassword;
                        if ((password == "" || password == undefined) && (confirmpassword == "" || confirmpassword == undefined)) {
                            $scope.showpass = true;
                            $scope.passwordMess = "Please enter New Password";
                            $scope.showconfirmpass = true;
                            $scope.confirmpwdMess = "Please enter confirm Password";
                            return false;
                        } else if ((password == "" || password == undefined)) {
                            $scope.showpass = true;
                            $scope.passwordMess = "Please enter New Password";
                            return false;
                        } else if ((confirmpassword == "" || confirmpassword == undefined)) {
                            $scope.showconfirmpass = true;
                            $scope.confirmpwdMess = "Please enter confirm Password";
                            return false;
                        } else {
                            return false;
                        }

                    } else {
                        $scope.showpass = true;
                        $scope.passwordMess = "Please enter New Password";
                        $scope.showconfirmpass = true;
                        $scope.confirmpwdMess = "Please enter confirm Password";
                        return false;
                        return false;
                    }
                }

                $scope.click_pass = function(){
                    $scope.showpass = false;
                    $("#eyeIcon").show();
                }
                $scope.click_confirmpass = function(){
                    $scope.showconfirmpass = false;
                }
                $("#password").keypress(function (e) {
                    var code = e.keyCode || e.which;
                    if (code == 13) { //Enter keycode
                        $scope.changePwd($scope.user);
                        document.activeElement.blur();
                    }
                 });


                $("#confirmpassword").keypress(function (e) {
                    var code = e.keyCode || e.which;
                    if (code == 13) { //Enter keycode
                        $scope.changePwd($scope.user);
                        document.activeElement.blur();
                    }
                });

    } catch (e) {
        console.log(ew);
    }
}]);
