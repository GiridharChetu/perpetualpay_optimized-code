//'use strict';
appAGMS.controller('FriendInviteCtrl', ['$scope', '$rootScope', '$location', '$route', 'AgmsFactory', function ($scope, $rootScope, $location, $route, AgmsFactory) {

    try {
        var businessArry = 0;
        var phonelist = 0;
        var list = 0;
        currentPage = "InvitePage";


        //............for showing Phonelist  .............//
        var init = function () {

             $scope.getbusinessInfo();
            function onSuccess(contacts) {
                $scope.allContacts = contacts;
                sessionStorage["Contactlist"] = JSON.stringify($scope.allContacts);
               if (sessionStorage["Contactlist"] != undefined) {
                    $scope.Contactlist = $scope.allContacts;
                     $('#loader').hide();
                    $scope.$apply();

               }
            }

            function onError(contactError) {
                $('#loader').hide();
                if(contactError==20){
                AgmsFactory.showToast(showcontactlistMSG,7000);
                }
//                AgmsFactory.showToast(JSON.stringify(contactError));

            };

            //...... find all contacts.........//
            if(sessionStorage["Contactlist"] != undefined && sessionStorage["Contactlist"] != ""){
                $scope.allContacts  = JSON.parse(sessionStorage["Contactlist"]);
            }
            else{
            $('#loader').show();
            var options = new ContactFindOptions();

            options.filter = "";
            options.multiple = true;
            filter = ["displayName", "addresses"];
            navigator.contacts.find(filter, onSuccess, onError, options);
            }


        }
        $scope.getbusinessInfo = function () {

            $scope.business = JSON.parse(localStorage["businessData"]);
            for (list; list <= $scope.business.data.relationships.length; list++) {
                if ($scope.business.data.relationships[list].sfid == sessionStorage["SelectedSFID"]) {
                    $scope.businesslogo = $scope.business.data.relationships[list].business_logo__c;
                    $scope.displayname = $scope.business.data.relationships[list].display_name__c;
                    $scope.reward = $scope.business.data.relationships[list].rewards;
                    $scope.business_sfid = $scope.business.data.relationships[list].sfid;
                    break;
                }
            }
        }
        init();
        //............for calling invite api   .............//
        $scope.inviteContact = function (Mobileno) {

            if (AgmsFactory.getDeviceConnectivity() != "none") {
                $('#loader').show();
                $scope.AllData = JSON.parse(localStorage["businessData"]);
                $scope.referid = $scope.AllData.data.sfid;
                for (businessArry; businessArry <= $scope.AllData.data.relationships.length; businessArry++) {
                    $scope.businessid = $scope.AllData.data.relationships[businessArry].sfid;
                    break;
                }
                $scope.inviteBody = {
                    "referred_by_id": $scope.referid,
                    "phone_number": Mobileno,
                    "business_id": $scope.businessid
                }

                AgmsFactory.getInvite($scope.inviteBody)
                    .success(function ($data) {
                        $('#loader').hide();
                        AgmsFactory.showToast(JSON.stringify($data.errorMessage));
                    }).error(function (data) {
                        $('#loader').hide();
                        AgmsFactory.showToast(JSON.stringify($data.errorMessage));
                       if(data.errorMessage=="Bad or expired token."){
                           AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                           $location.path("/Login");
                       }
                       else{
                            AgmsFactory.showToast(data.errorMessage, 4000);

                       }

                    });
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }
        }
        //............for calling invite api   .............//

        //............for back button  .............//
                $scope.backbtn = function () {
                 $rootScope.StopInterval = "false";
                    $location.path("/AppLayout");
                }
                $scope.facebooklist = function () {
                   sessionStorage["SelectedSFID"] =$scope.business_sfid;
                   $location.path("/facebooklist");
                }

    }
    catch (e) {
        console.log(e);
    }

}]);
