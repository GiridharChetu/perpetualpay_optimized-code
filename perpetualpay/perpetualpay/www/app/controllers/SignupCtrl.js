
appAGMS.controller('SignupCtrl', ['$scope', '$rootScope', '$location', '$route', 'AgmsFactory', function ($scope, $rootScope, $location, $route, AgmsFactory) {

    try {

    $scope.user = {
              fName : "",
              lName : "",
              uName : "",
              passWord : ""
     };
     currentPage = "SignupPage";

    var init = function(){
          $scope.editMode = false;
          $("#eyeIcon").hide();

         $scope.showeyeIcon = true;
         $scope.hideeyeIcon = false;
  }
         init();

 //...........call create api for new user signup............//
       $scope.CreateNewUser = function(user){

             var fName = user.ffrstname;
             var lName = user.LastName;
             var uName = user.UserName;
             var passWord = user.Password;

             if (AgmsFactory.getDeviceConnectivity() != "none") {
                if((fName == "" || fName == undefined) && (lName == "" || lName == undefined) && (uName == "" || uName == undefined) && (passWord == "" || passWord == undefined)
                || (fName == "" || fName == undefined) ||(lName == "" || lName == undefined) ||(uName == "" || uName == undefined) ||(passWord == "" || passWord == undefined))
                {
                    $scope.isValidated($scope.user);
                }
                else{

                     $scope.fn = $scope.user.ffrstname;
                     $scope.ln = $scope.user.LastName;
                     $scope.un = $scope.user.UserName;
                     $scope.pwd = $scope.user.Password;

                    $scope.createnewSignup = {
                        "referred_by_phone_number":"0000000000",
                        "phone_number":sessionStorage["phone_number"],
                        "business_username":sessionStorage["businessName"],
                        "first_name":$scope.fn,
                        "last_name":$scope.ln,
                        "username":$scope.un,
                        "password":$scope.pwd
                        };
                       // alert($scope.createnewSignup);
                        $('#loader').show();
                    AgmsFactory.CreateSignupNewUser($scope.createnewSignup)
                      .success(function (data) {
                            $('#loader').hide();
                         if (!data.error) {
                            refreshToken = data.refresh_token;
                            token = data.token;
                            localStorage["Token"] = token;
                            localStorage["refreshToken"] = refreshToken;

                           $scope.user.ffrstname = null;
                           $scope.user.LastName = null;
                           $scope.user.UserName = null;
                           $scope.user.Password = null;
                           $scope.placevalue = null;
                            $rootScope.StopInterval = "false";
                           $location.path("/AppLayout");

                         } else {
                            $('#loader').hide();
                            AgmsFactory.showToast(data.errorMessage, 7000);
        //                   $scope.user.ffrstname = null;
        //                   $scope.user.LastName = null;
        //                   $scope.user.UserName = null;
        //                   $scope.user.Password = null;
                            }

                       })
                        .error(function (data) {
                            $('#loader').hide();
                            AgmsFactory.showToast(data.errorMessage,7000);
                            if(data.errorMessage=="Bad or expired token."){
                                AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                $location.path("/Login");
                            }
                            else{
                                 AgmsFactory.showToast(data.errorMessage, 4000);

                            }
                        });
                    }
             }
           else{
                AgmsFactory.showToast(NoInterNet, 7000);
           }
       }
    //...........call create api for new user signup.....................//

    //.....................validation method.............................//
            $scope.isValidated = function (user) {
                if (user != undefined) {
                    var fName = user.ffrstname;
                    var lName = user.LastName;
                    var uName = user.UserName;
                    var passWord = user.Password;

                    if ((fName == "" || fName == undefined) && (lName == "" || lName == undefined) && (uName == "" || uName == undefined) && (passWord == "" || passWord == undefined)) {
                        $scope.showfirstname = true;
                        $scope.FNValidMess = signup_fn;
                        $scope.showlasttname = true;
                        $scope.LNValidMess = signup_ln;
                        $scope.showUsername = true;
                        $scope.UNValidMess = sign_un;
                        $scope.showpassword = true;
                        $scope.passwordValidMess = signup_pwd;
                        return false;
                    } else if ((fName == "" || fName == undefined)) {
                         $scope.showfirstname = true;
                        $scope.FNValidMess = signup_fn;
                        return false;
                    } else if ((lName == "" || lName == undefined)) {
                        $scope.showlasttname = true;
                        $scope.LNValidMess = signup_ln;
                        return false;
                     } else if ((uName == "" || uName == undefined)) {
                         $scope.showUsername = true;
                         $scope.UNValidMess = sign_un;
                         return false;
                     } else if ((passWord == "" || passWord == undefined)) {
                         $scope.showpassword = true;
                         $scope.passwordValidMess = signup_pwd;
                         return false;
                     }
                    else {
                        return false;
                    }

                } else {
                    $scope.showfirstname = true;
                    $scope.FNValidMess = signup_fn;
                    $scope.showlasttname = true;
                    $scope.LNValidMess = signup_ln;
                    $scope.showUsername = true;
                    $scope.UNValidMess = sign_un;
                    $scope.showpassword = true;
                    $scope.passwordValidMess = signup_pwd;
                    return false;
                    return false;
                    return false;
                    return false;
                }

            }

            $scope.firstname_Click = function () {
                 $scope.showfirstname = false;
            }
            $scope.lastname_Click = function () {
                $scope.showlasttname = false;
            }
            $scope.username_Click = function () {
               $scope.showUsername = false;
            }

            $scope.password_Click = function () {
                $scope.placevalue = "**********";
                $("#eyeIcon").show();
//                $scope.pwd = true;
                $scope.showpassword = false;
            }

       //......................validation method.............................//

        // Set the default value of inputType
          $scope.inputType = 'password';

            // Hide & show password function
            $scope.hideShowPassword = function(){
              if ($scope.inputType == 'password'){
              $("#Passwordlel").addClass("active");
                $scope.inputType = 'text';
                $scope.hideeyeIcon = true;
                $scope.showeyeIcon = false;

              }
              else{
              $("#Passwordlel").addClass("active");
                $scope.inputType = 'password';
                $scope.hideeyeIcon = false;
                $scope.showeyeIcon = true;

              }
            };


        // check api response
        $rootScope.message = "";
        $scope.Login = function () {
            $location.path("/Login");
        }

        $("#password").keypress(function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) { //Enter keycode
                $scope.CreateNewUser($scope.user);
                document.activeElement.blur();
            }
        });

      }
    catch (e) {
        console.log(e);
    }

}]);
