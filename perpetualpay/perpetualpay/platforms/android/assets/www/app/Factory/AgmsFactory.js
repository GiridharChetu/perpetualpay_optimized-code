'use strict';
angular.module('app')
    .factory('AgmsFactory', ['$http', '$location','$rootScope', function ($http, $location,$rootScope) {
        var url = "";
        var Factory = [];


        Factory.authUser = function (user) {
            url = apiBaseAddress + "/auth";

             return $http.post(url,
                          user,{
                                headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                            });

        };

        Factory.getUser = function () {
            token =localStorage["Token"];
            url = apiBaseAddress + "/" + token + "/users/self";

               return $http.get(url,{
                                   headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                  });

        };
        Factory.changePassword = function (user) {
            url = apiBaseAddress + "/" + token + "/users/self/update";

               return $http.put(url,
                                user,{
                                  headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                                 });

        };
        Factory.getInvite = function (inviteBody) {
            url = apiBaseAddress + "/" + token + "/users/self/invite";
                return $http.post(url,
                             inviteBody,{
                                 headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                            });

        };
        Factory.getverifySingnupUser = function (verifysignupdata) {
            url = apiBaseAddress + "/" + newtoken + "/system/self/verify";

                 return $http.post(url,
                              verifysignupdata,{
                                  headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                              });

        };
        Factory.CreateSignupNewUser = function (createnewSignup) {
            url = apiBaseAddress + "/" + newtoken + "/system/self/create";
                  return $http.post(url,
                             createnewSignup,{
                                     headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                             });

        };
        Factory.getSignupAuthkey = function (defaultUser) {
             url = apiBaseAddress +  "/auth";
            // alert(url);
                  return $http.post(url,
                             defaultUser,{
                                  headers:{'Authorization': apiSecreatKey,'Content-Type': 'application/json'}
                             });

         };
         Factory.updateProfile = function (Profile_url) {
             url = "https://api.imgur.com/3/upload";

           return $http.post(url,
              Profile_url,{
                    headers:{'Authorization': 'Client-ID 072dcaa8ee9f00a','Content-Type': 'application/json'}
                });


         };

         //----------- use in case of website url is not including https-----------//
         Factory.UseSlice = function(url){
            if(url != "" && url != undefined){
             var websiteURl = url.slice("https");
             }
            return websiteURl;
         };

        //----------- Show toast message-----------//
        Factory.showToast = function (message, duration) {
            document.addEventListener("deviceready", onDeviceReady, false);

            function onDeviceReady() {
                window.plugins.toast.showWithOptions({
                    message: message,
                    duration: duration, // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
                    position: "bottom",
                    addPixelsY: -40 // added a negative value to move it up a bit (default 0)
                });
            }
        };
        //----------- Device connectivity-----------//
        Factory.getDeviceConnectivity = function () {
            var networkState = navigator.connection.type;
            return networkState;
        };
        //----------- Device platform--------------//
            Factory.getdeviceplatform =  function(){
              document.addEventListener("deviceready", onDeviceReady, false);
                function onDeviceReady() {
                     var string = device.platform;
                       return string;
                 }
            }

        //----------- find Current Lat Long-----------//
        Factory.getCurrentlatLong = function () {
                    if ("geolocation" in navigator) { //check geolocation available
                        //try to get user current location using getCurrentPosition() method
                        navigator.geolocation.getCurrentPosition(function (position) {
                            //alert("Lat :" +position.coords.latitude +"','" + "Long :" +position.coords.longitude);
                            sessionStorage["Currentlatitude"] = position.coords.latitude;
                            sessionStorage["Currentlogitude"] = position.coords.longitude;
                        });
                    }
                    else {
                        alert("Browser doesn't support geolocation!");
                    }
        };

         //----------- distance between two Lat Long-----------//
         Factory.LatlongcalculateDistance =  function(lat1, long1, lat2, long2,unit){

                        var radlat1 = Math.PI * lat1/180;
                        var radlat2 = Math.PI * lat2/180;
                        var theta = long1-long2;
                        var radtheta = Math.PI * theta/180;
                        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                        dist = Math.acos(dist);
                        dist = dist * 180/Math.PI;
                        dist = dist * 60 * 1.1515;

                        if (unit=="K") { dist = dist * 1.609344 }
                        if (unit=="N")  { dist = dist * 0.8684 }
                        if (unit =="F"){ dist = dist * 5280}
                        return dist
        };

        return Factory;


  }]);
