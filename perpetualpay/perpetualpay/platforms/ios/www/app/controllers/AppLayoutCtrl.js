'use strict';

appAGMS.controller("AppLayoutCtrl", ["$scope", "$rootScope", "$location", "$route", "AgmsFactory", function ($scope, $rootScope, $location, $route, AgmsFactory, $window) {

    try {
        $scope.darkcolor = "Dark";
        $scope.lightcolor = "Light";
        $scope.weblink = "https://www.agms.com";
        currentPage = "Dashboard";
        var screenNo = 0;
        var i = 0;
        var init = function () {

            $scope.hiddenloader = true;
            $scope.refreshLoader = false;

            $rootScope.message = "";
            $scope.prevhide();

            $scope.checkStatus();

            //............for calling share icon popup hide .............//
            $scope.hidePopup = function () {
                $('#modal1').hide();
            }
        }

        //............for changing the pre icon color .............//
        $scope.prevhide = function () {
            if (lightcolor == textcolor) {
                return "prev-icon icon-light";
            } else {
                return "prev-icon icon-dark";
            }
        }

        //............for calling business layout page .............//
        $scope.Screens = function () {
            if (AgmsFactory.getdeviceplatform() == "Android" || AgmsFactory.getdeviceplatform() == "android") {
                if (AgmsFactory.getDeviceConnectivity() != "none") {
                    if ($scope.hiddenloader == false) {
                        $('#loader').hide();
                    } else {
                        $('#loader').show();
                    }
                    $scope.user = null;
                    $scope.editMode = false;
                    if (screenNo == 0)
                        $scope.hideprevIcon = false;
                    else
                        $scope.hideprevIcon = true;
                    AgmsFactory.getUser().success(function ($data) {
                        if ($data.error == true) {
                            $('#loader').hide();
                            AgmsFactory.showToast($data.errorMessage, 4000);
                            $location.path("/Login");
                            return;
                        }
                        if ($data.errorMessage == "Bad or expired token.") {
                            $('#loader').hide();
                            AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                            $location.path("/Login");
                            return;
                        }
                        if ($data.data.relationships.length == 0) {
                            $('#loader').hide();
                            AgmsFactory.showToast("No Business Available", 4000);
                            $location.path("/Login");
                            return;
                        }



                        //else{
                        localStorage["businessData"] = JSON.stringify($data);
                        $scope.businessData = $data.data;
                        $scope.phone = $data.data.sign_in_phone__c;
                        localStorage["relationshipData"] = $scope.businessData.relationships;
                        $('#loader').hide();
                        $scope.refreshLoader = false;

                        if (screenNo >= $data.data.relationships.length - 1) {
                            $scope.hidenextIcon = false;
                        } else {
                            $scope.hidenextIcon = true;
                        }
                        if (screenNo <= $data.data.relationships.length) {
                            $scope.reward = $data.data.relationships[screenNo].rewards;
                            $scope.color = $data.data.relationships[screenNo].card_color__c;
                            $scope.logo = $data.data.relationships[screenNo].business_logo__c;
                            $scope.facebook = $data.data.relationships[screenNo].facebook__c;
                            $scope.twitter = $data.data.relationships[screenNo].twitter__c;
                            $scope.mobile = $data.data.relationships[screenNo].sign_in_phone__c;
                            $scope.direct = $data.data.relationships[screenNo].referrals.direct;
                            $scope.initialrefrel = $data.data.relationships[screenNo].referrals.initial;
                            $scope.description = $data.data.relationships[screenNo].description;
                            $scope.street = $data.data.relationships[screenNo].shippingstreet;
                            $scope.city = $data.data.relationships[screenNo].shippingcity;
                            $scope.state = $data.data.relationships[screenNo].shippingstate;
                            $scope.code = $data.data.relationships[screenNo].shippingpostalcode;
                            $scope.textcolor = $data.data.relationships[screenNo].text_color__c;
                            $scope.displayName = $data.data.relationships[screenNo].display_name__c;
                            $scope.website = $data.data.relationships[screenNo].website;
                            $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                            $scope.displayName = $data.data.relationships[screenNo].display_name__c;
                            $scope.sfid = $data.data.relationships[screenNo].sfid;
                            $scope.business_username = $data.data.relationships[screenNo].username__c
                            $scope.userfullname = $data.data.name;

                            var phoneformat = $scope.mobile;
                            $scope.USPhoneNumber = phoneformat.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3");

                            $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;
                            sessionStorage["address"] = JSON.stringify($scope.Address);


                            var stop = $rootScope.StopInterval;

                            if (stop == undefined || stop != "true")
                                myTimer = setInterval($scope.getAddresslatlong(0), 5000);
                        }
                        // }
                    }).error(function (data) {
                        $('#loader').hide();
                        $scope.refreshLoader = false;
                        if (data.errorMessage == "Bad or expired token.") {
                            AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                            $location.path("/Login");
                        } else {
                            AgmsFactory.showToast(data.errorMessage, 4000);

                        }
                    });


                } else {
                    AgmsFactory.showToast(NoInterNet, 7000);
                }
            } else {

                if ($scope.hiddenloader == false) {
                    $('#loader').hide();
                } else {
                    $('#loader').show();
                }
                $scope.user = null;
                $scope.editMode = false;
                if (screenNo == 0)
                    $scope.hideprevIcon = false;
                else
                    $scope.hideprevIcon = true;
                AgmsFactory.getUser().success(function ($data) {
                    if ($data.error == true) {
                        $('#loader').hide();
                        AgmsFactory.showToast($data.errorMessage, 4000);
                        $location.path("/Login");
                        return;
                    }
                    if ($data.errorMessage == "Bad or expired token.") {
                        $('#loader').hide();
                        AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                        $location.path("/Login");
                        return;
                    }
                    if ($data.data.relationships.length == 0) {
                        $('#loader').hide();
                        AgmsFactory.showToast("No Business Available", 4000);
                        $location.path("/Login");
                        return;
                    }



                    //else{
                    localStorage["businessData"] = JSON.stringify($data);
                    $scope.businessData = $data.data;
                     $scope.phone = $data.data.sign_in_phone__c;
                    localStorage["relationshipData"] = $scope.businessData.relationships;
                    $('#loader').hide();
                    $scope.refreshLoader = false;

                    if (screenNo >= $data.data.relationships.length - 1) {
                        $scope.hidenextIcon = false;
                    } else {
                        $scope.hidenextIcon = true;
                    }
                    if (screenNo <= $data.data.relationships.length) {
                        $scope.reward = $data.data.relationships[screenNo].rewards;
                        $scope.color = $data.data.relationships[screenNo].card_color__c;
                        $scope.logo = $data.data.relationships[screenNo].business_logo__c;
                        $scope.facebook = $data.data.relationships[screenNo].facebook__c;
                        $scope.twitter = $data.data.relationships[screenNo].twitter__c;
                        $scope.mobile = $data.data.relationships[screenNo].sign_in_phone__c;
                        $scope.direct = $data.data.relationships[screenNo].referrals.direct;
                        $scope.initialrefrel = $data.data.relationships[screenNo].referrals.initial;
                        $scope.description = $data.data.relationships[screenNo].description;
                        $scope.street = $data.data.relationships[screenNo].shippingstreet;
                        $scope.city = $data.data.relationships[screenNo].shippingcity;
                        $scope.state = $data.data.relationships[screenNo].shippingstate;
                        $scope.code = $data.data.relationships[screenNo].shippingpostalcode;
                        $scope.textcolor = $data.data.relationships[screenNo].text_color__c;
                        $scope.displayName = $data.data.relationships[screenNo].display_name__c;
                        $scope.website = $data.data.relationships[screenNo].website;
                        $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                        $scope.displayName = $data.data.relationships[screenNo].display_name__c;
                        $scope.sfid = $data.data.relationships[screenNo].sfid;
                        $scope.business_username = $data.data.relationships[screenNo].username__c
                        $scope.userfullname = $data.data.name;

                        var phoneformat = $scope.mobile;
                        $scope.USPhoneNumber = phoneformat.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3");

                        $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;
                        sessionStorage["address"] = JSON.stringify($scope.Address);


                        var stop = $rootScope.StopInterval;

                        if (stop == undefined || stop != "true")
                            myTimer = setInterval($scope.getAddresslatlong(0), 5000);
                    }
                    // }
                }).error(function (data) {
                    $('#loader').hide();
                    $scope.refreshLoader = false;
                    if (data.errorMessage == "Bad or expired token.") {
                        AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                        $location.path("/Login");
                    } else {
                        AgmsFactory.showToast(data.errorMessage, 4000);

                    }
                });

            }
        };


        //.................call when click on back button on every page..............//
        $scope.checkStatus = function () {
            if (AgmsFactory.getdeviceplatform() == "Android" || AgmsFactory.getdeviceplatform() == "android") {
                if (AgmsFactory.getDeviceConnectivity() != "none") {
                    if (sessionStorage["SelectedSFID"] != undefined && sessionStorage["SelectedSFID"] != '') {
                        //$('#loader').hide();
                        $scope.refreshLoader = false;
                        $scope.phone = JSON.parse(localStorage["businessData"]).data.sign_in_phone__c;
                        $scope.businessData = JSON.parse(localStorage["businessData"]).data;
                        $scope.Relationship = JSON.parse(localStorage["businessData"]).data.relationships;

                        for (var infolength = 0; infolength < $scope.Relationship.length; infolength++) {
                            if ($scope.Relationship[infolength].sfid == sessionStorage["SelectedSFID"]) {
                                $scope.reward = $scope.Relationship[infolength].rewards;
                                $scope.color = $scope.Relationship[infolength].card_color__c;
                                $scope.logo = $scope.Relationship[infolength].business_logo__c;
                                $scope.facebook = $scope.Relationship[infolength].facebook__c;
                                $scope.twitter = $scope.Relationship[infolength].twitter__c;
                                $scope.mobile = $scope.Relationship[infolength].sign_in_phone__c;
                                $scope.direct = $scope.Relationship[infolength].referrals.direct;
                                $scope.initialrefrel = $scope.Relationship[infolength].referrals.initial;
                                $scope.description = $scope.Relationship[infolength].description;
                                $scope.street = $scope.Relationship[infolength].shippingstreet;
                                $scope.city = $scope.Relationship[infolength].shippingcity;
                                $scope.state = $scope.Relationship[infolength].shippingstate;
                                $scope.code = $scope.Relationship[infolength].shippingpostalcode;
                                $scope.textcolor = $scope.Relationship[infolength].text_color__c;
                                $scope.displayName = $scope.Relationship[infolength].display_name__c;
                                $scope.website = $scope.Relationship[infolength].website;
                                $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                                $scope.displayName = $scope.Relationship[infolength].display_name__c;
                                $scope.sfid = $scope.Relationship[infolength].sfid;
                                $scope.business_username = $scope.Relationship[infolength].username__c
                                $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;
                                sessionStorage["address"] = JSON.stringify($scope.Address);
                                $scope.userfullname = $scope.businessData.name;
                                var phoneformat = $scope.mobile;
                                $scope.USPhoneNumber = phoneformat.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3");

                                if (infolength <= 0)
                                    $scope.hideprevIcon = false;
                                else
                                    $scope.hideprevIcon = true;
                                if (infolength >= $scope.Relationship.length - 1) {
                                    $scope.hidenextIcon = false;
                                } else {
                                    $scope.hidenextIcon = true;
                                }
                                screenNo = infolength;

                            }
                        }
                    } else {
                        $scope.Screens();
                    }
                } else {
                    AgmsFactory.showToast(NoInterNet, 7000);
                }
            } else {

                if (sessionStorage["SelectedSFID"] != undefined && sessionStorage["SelectedSFID"] != '') {
                    //$('#loader').hide();
                    $scope.refreshLoader = false;
                    $scope.phone = JSON.parse(localStorage["businessData"]).data.sign_in_phone__c;
                    $scope.businessData = JSON.parse(localStorage["businessData"]).data;
                    $scope.Relationship = JSON.parse(localStorage["businessData"]).data.relationships;

                    for (var infolength = 0; infolength < $scope.Relationship.length; infolength++) {
                        if ($scope.Relationship[infolength].sfid == sessionStorage["SelectedSFID"]) {
                            $scope.reward = $scope.Relationship[infolength].rewards;
                            $scope.color = $scope.Relationship[infolength].card_color__c;
                            $scope.logo = $scope.Relationship[infolength].business_logo__c;
                            $scope.facebook = $scope.Relationship[infolength].facebook__c;
                            $scope.twitter = $scope.Relationship[infolength].twitter__c;
                            $scope.mobile = $scope.Relationship[infolength].sign_in_phone__c;
                            $scope.direct = $scope.Relationship[infolength].referrals.direct;
                            $scope.initialrefrel = $scope.Relationship[infolength].referrals.initial;
                            $scope.description = $scope.Relationship[infolength].description;
                            $scope.street = $scope.Relationship[infolength].shippingstreet;
                            $scope.city = $scope.Relationship[infolength].shippingcity;
                            $scope.state = $scope.Relationship[infolength].shippingstate;
                            $scope.code = $scope.Relationship[infolength].shippingpostalcode;
                            $scope.textcolor = $scope.Relationship[infolength].text_color__c;
                            $scope.displayName = $scope.Relationship[infolength].display_name__c;
                            $scope.website = $scope.Relationship[infolength].website;
                            $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                            $scope.displayName = $scope.Relationship[infolength].display_name__c;
                            $scope.sfid = $scope.Relationship[infolength].sfid;
                            $scope.business_username = $scope.Relationship[infolength].username__c
                            $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;
                            sessionStorage["address"] = JSON.stringify($scope.Address);
                            $scope.userfullname = $scope.businessData.name;
                            var phoneformat = $scope.mobile;
                            $scope.USPhoneNumber = phoneformat.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3");

                            if (infolength <= 0)
                                $scope.hideprevIcon = false;
                            else
                                $scope.hideprevIcon = true;
                            if (infolength >= $scope.Relationship.length - 1) {
                                $scope.hidenextIcon = false;
                            } else {
                                $scope.hidenextIcon = true;
                            }
                            screenNo = infolength;

                        }
                    }
                } else {
                    $scope.Screens();
                }


            }
        }
        //.................call when click on back button on every page..............//


        //............for calling function on next icon .............//
        $scope.ScreensNext = function () {
            if (screenNo == JSON.parse(localStorage["businessData"]).data.relationships.length - 1) {
                return false;
            }
            $scope.user = null;
            setTimeout(myFunction, 1000);
            $scope.editMode = false;
            if (screenNo <= -1)
                $scope.hideprevIcon = false;
            else
                $scope.hideprevIcon = true;
            screenNo++;
            $scope.phone = JSON.parse(localStorage["businessData"]).data.sign_in_phone__c;
            $scope.Relationship = JSON.parse(localStorage["businessData"]).data.relationships;
            if (screenNo >= $scope.Relationship.length - 1) {
                $scope.hidenextIcon = false;
            } else {
                $scope.hidenextIcon = true;
            }
            if (screenNo <= $scope.Relationship.length) {
                $(".orange-bg").addClass('animated slideInRight');
                $scope.reward = $scope.Relationship[screenNo].rewards;
                $scope.color = $scope.Relationship[screenNo].card_color__c;
                $scope.logo = $scope.Relationship[screenNo].business_logo__c;
                $scope.facebook = $scope.Relationship[screenNo].facebook__c;
                $scope.twitter = $scope.Relationship[screenNo].twitter__c;
                $scope.mobile = $scope.Relationship[screenNo].sign_in_phone__c;
                $scope.direct = $scope.Relationship[screenNo].referrals.direct;
                $scope.initialrefrel = $scope.Relationship[screenNo].referrals.initial;
                $scope.description = $scope.Relationship[screenNo].description;
                $scope.street = $scope.Relationship[screenNo].shippingstreet;
                $scope.city = $scope.Relationship[screenNo].shippingcity;
                $scope.state = $scope.Relationship[screenNo].shippingstate;
                $scope.code = $scope.Relationship[screenNo].shippingpostalcode;
                $scope.textcolor = $scope.Relationship[screenNo].text_color__c;
                $scope.displayName = $scope.Relationship[screenNo].display_name__c;
                $scope.website = $scope.Relationship[screenNo].website;

                $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                var phoneformat = $scope.mobile;
                $scope.USPhoneNumber = phoneformat.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3");

                $scope.sfid = $scope.Relationship[screenNo].sfid;
                $scope.business_username = $scope.Relationship[screenNo].username__c
                $scope.userfullname = $scope.businessData.name;

                $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;

            }

        };

        function myFunction() {
            $(".orange-bg").removeClass('animated slideInRight');
            $(".orange-bg").removeClass(' animated slideInLeft');
        }

        //.......................for calling function on previous icon .....................//
        $scope.ScreenRev = function () {
            if (screenNo == 0) {
                return false;
            }
            setTimeout(myFunction, 1000);
            $scope.user = null;
            $scope.editMode = false;
            screenNo--;
            if (screenNo <= 0)
                $scope.hideprevIcon = false;
            else
                $scope.hideprevIcon = true;
            $scope.phone = JSON.parse(localStorage["businessData"]).data.sign_in_phone__c;
            $scope.Relationship = JSON.parse(localStorage["businessData"]).data.relationships;
            if (screenNo >= $scope.Relationship.length - 1) {
                $scope.hidenextIcon = false;
            } else {
                $scope.hidenextIcon = true;
            }
            if (screenNo <= $scope.Relationship.length) {
                $(".orange-bg").addClass('animated slideInLeft');
                $scope.reward = $scope.Relationship[screenNo].rewards;
                $scope.color = $scope.Relationship[screenNo].card_color__c;
                $scope.logo = $scope.Relationship[screenNo].business_logo__c;
                $scope.facebook = $scope.Relationship[screenNo].facebook__c;
                $scope.twitter = $scope.Relationship[screenNo].twitter__c;
                $scope.mobile = $scope.Relationship[screenNo].sign_in_phone__c;
                $scope.direct = $scope.Relationship[screenNo].referrals.direct;
                $scope.initialrefrel = $scope.Relationship[screenNo].referrals.initial;
                $scope.description = $scope.Relationship[screenNo].description;
                $scope.textcolor = $scope.Relationship[screenNo].text_color__c;
                $scope.displayName = $scope.Relationship[screenNo].display_name__c;
                $scope.website = $scope.Relationship[screenNo].website;
                $scope.websiteURL = AgmsFactory.UseSlice($scope.website);
                $scope.displayName = $scope.Relationship[screenNo].display_name__c;
                $scope.street = $scope.Relationship[screenNo].shippingstreet;
                $scope.city = $scope.Relationship[screenNo].shippingcity;
                $scope.state = $scope.Relationship[screenNo].shippingstate;
                $scope.code = $scope.Relationship[screenNo].shippingpostalcode;
                $scope.sfid = $scope.Relationship[screenNo].sfid;
                $scope.business_username = $scope.Relationship[screenNo].username__c
                $scope.userfullname = $scope.businessData.name;
                var phoneformat = $scope.mobile;
                $scope.USPhoneNumber = phoneformat.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3");


                $scope.Address = $scope.street + "," + $scope.city + "," + $scope.state + "," + $scope.code;

            }
        };

        init();
        // check api response 
        $rootScope.message = "";


        $scope.backbtn = function () {
            clearInterval(myTimer);
            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/AppLayout");
        }


        //............for calling OR code icon popup.............//
        $scope.QRcode = function () {

            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/QRCode");
        }
        //............for calling invite icon popup.............//
        $scope.FriendInvite = function () {
            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/FriendInvite");
        }
        //............for calling setting icon popup.............//
        $scope.Accounts = function () {
            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/Accounts");
        }

        //............for calling i icon popup show.............//
        $scope.showmodel = function () {
            $('#modal1').show();

        }
        //............for i icon popup show.............//
        $scope.modelChng = function () {
            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/BusinessInformation");
            //            $('#modalAccountOrange').show();

        }
        //............for i icon popup hide.............//
        $scope.modelChngHide = function () {
            sessionStorage["SelectedSFID"] = $scope.sfid;
            $location.path("/AppLayout");
            //            $('#modalAccountOrange').hide();

        }

        //............calling for Phone icon.............//

        function dialNumber() {
            var Phoneno = $scope.mobile;
            window.plugins.CallNumber.callNumber(onSuccess, onError, Phoneno, true);
        }

        function onSuccess(data) {
            // AgmsFactory.showToast("Success: "+JSON.stringify(data));
        }

        function onError(data) {
            if (data.Error == 20) {
                AgmsFactory.showToast(connect_callMSG, 7000);
            } else {
                AgmsFactory.showToast(check_simcardMSG, 7000);
            }
        }


        //............show popup for Phone dialer.............//
        $scope.confirmDialogbox = function () {
            if (AgmsFactory.getdeviceplatform() == "Android" || AgmsFactory.getdeviceplatform() == "android") {

                function onSwitchConfirm(buttonIndex) {
                    if (buttonIndex == 1) {

                        dialNumber();
                    } else {
                        return false;
                    }

                }
                navigator.notification.confirm(
                    $scope.USPhoneNumber, // message
                    onSwitchConfirm, // callback to invoke with index of button pressed
                    "", // title

                    ['CALL', 'CANCEL'] // buttonLabels

                );
            }
            else {
                var number = $scope.mobile.replace(/^(\d{3})(\d{3})(\d{4})+$/, "($1) $2-$3").toString();
                location.href = "tel:" + number;
            }
        }
        //............share Facebook.............//
        $scope.socialSharing = function () {
            //            msg = 'I have earned $' + $scope.reward + ' at ' + $scope.displayName + ' using Perpatual Pay! Earn your own rewards today!';
            msg = $scope.userfullname + " has invited you to " + $scope.displayName + ". Follow the link to create your Perpetual Pay account to start earning rewards:\nhttps://www.perpetualpay.com/invite/" + $scope.business_username + "/" + $scope.phone;
            Image = $scope.logo;

            //window.plugins.socialsharing.share(msg);
            window.plugins.socialsharing.share(msg, null, null, null);
        }


        //............share twitter.............//
        $scope.shareViaTwitter = function () {
            msg = 'I have earned $' + $scope.reward + ' at ' + $scope.displayName + ' using Perpatual Pay! Earn your own rewards today!';
            Image = $scope.logo;
            window.plugins.socialsharing.shareViaTwitter(
                msg, [Image],
                null);
        }


        //----------- Lat Long by address-----------//
        $scope.getAddresslatlong = function (b) {

            var Data = JSON.parse(localStorage["businessData"]);
            if (Data.data.relationships.length > b) {
                var address = Data.data.relationships[b].shippingstreet + "," + Data.data.relationships[b].shippingcity + "," + Data.data.relationships[b].shippingstate + "," + Data.data.relationships[b].shippingpostalcode;
                if (address != "null,null,null,null") {
                    if ("geolocation" in navigator) { //check geolocation available
                        //try to get user current location using getCurrentPosition() method
                        navigator.geolocation.getCurrentPosition(function (position) {
                            sessionStorage["Currentlatitude"] = position.coords.latitude;
                            sessionStorage["Currentlogitude"] = position.coords.longitude;
                            var geocoder = new google.maps.Geocoder();
                            var Latitude = 0.000000;
                            var Longitude = 0.00000;
                            geocoder.geocode({
                                'address': address
                            }, function (results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    Latitude = results[0].geometry.location.lat();
                                    Longitude = results[0].geometry.location.lng();

                                    var distance = AgmsFactory.LatlongcalculateDistance(parseFloat(sessionStorage["Currentlatitude"]), parseFloat(sessionStorage["Currentlogitude"]), Latitude, Longitude, "F");

                                    if (distance < 50) {

                                        sessionStorage["SelectedSFID"] = Data.data.relationships[b].sfid;
                                        // alert(Data.data.relationships[b].sfid);
                                        document.location = '#/QRCode/';
                                    } else {
                                        $scope.getAddresslatlong(b + 1);
                                    }
                                } else {
                                    AgmsFactory.showToast("Request failed.", 7000);
                                }
                            });
                        });
                    } else {
                        AgmsFactory.showToast(GeolocationMsg, 7000);
                    }
                } else {
                    $scope.getAddresslatlong(b + 1);
                }
            }

        };


        //------------------------Pull to refresh Page-----------------------------//

        $("#refreshID").swipe({
            //              swipeUp:function(event, direction, distance, duration) {
            //                console.log("You swiped " + direction)
            //              },
            swipeDown: function (event, direction, distance, duration) {
                $scope.hiddenloader = false;
                $scope.refreshLoader = true;
                $scope.Screens();
            },
            click: function (event, target) {},
            threshold: 50,
            //              passive: true,
            allowPageScroll: "vertical"
        });


        //------------------------Pull to refresh Page-------------------------------//

    } catch (e) {
        console.log(e);
    }
}]);
