'use strict';
appAGMS.controller('PhoneNumberCtrl', ['$scope', '$rootScope', '$location', '$route', 'AgmsFactory', function ($scope, $rootScope, $location, $route, AgmsFactory) {
    try {
      $scope.user = {
                phoneNumber: ""
            };
        currentPage = "VerifyPhone";
        //..........fetching refferal number from URL and show on login textbox...........//
          if(sessionStorage["phoneno"] != "" && sessionStorage["phoneno"] !=undefined){
            if(sessionStorage["trueValue"] == "true"){
              $('#userPhone').addClass("active");
              $scope.user.phoneNumber=sessionStorage.getItem("phoneno");
            }
          }
       //..........fetching refferal number from URL and show on login textbox...........//

            var init = function(){
                $scope.getSignupAuthKey();
            }


        $scope.getSignupAuthKey = function(){

                    var Id = Id_signup;
                    var Pwd = Pwd_signup;
                    var defaultUser = {"username":Id,"password":Pwd};
                    if (AgmsFactory.getDeviceConnectivity() != "none") {
                    $('#loader').show();
                    AgmsFactory.getSignupAuthkey(defaultUser)
                    .success(function (data) {
                           $('#loader').hide();
                        if (!data.error) {
//                            refreshToken = data.refresh_token;
                            token  = data.token;
                            localStorage["Token"] = token;
                            $location.path("/PhoneNumber");

                        } else {
                            $('#loader').hide();
                            AgmsFactory.showToast(data.errorMessage, 4000);
                        }
                    })
                    .error(function (data) {
                        $('#loader').hide();
                        if(data.errorMessage=="Bad or expired token."){
                            AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                            $location.path("/Login");
                        }
                        else{
                             AgmsFactory.showToast(data.errorMessage, 4000);

                        }
                    });
                }
                else{
                    AgmsFactory.showToast(NoInterNet, 7000);
                 }
             }

        //............for getting login credentials .............//
        $scope.verifyPhoneNo = function () {
            var referred_by_phone_number = sessionStorage["referralNumber"];
            var phone_number = $scope.user.phoneNumber;
            sessionStorage["phone_number"] = phone_number;
            var business_username = sessionStorage["businessName"];


            if (AgmsFactory.getDeviceConnectivity() != "none") {
                if(phone_number == "" || phone_number == undefined){
                        $scope.validate();
                }
                else{
                    $scope.verifysignupdata = { "referred_by_phone_number": referred_by_phone_number, "phone_number": phone_number, "business_username": business_username};
                    //alert(JSON.stringify($scope.verifysignupdata));
                    $('#loader').show();
                    AgmsFactory.getverifySingnupUser($scope.verifysignupdata)
                            .success(function (data) {
                            //alert(JSON.stringify(data));
                                   $('#loader').hide();
                                    if(data.errorMessage=="Bad or expired token."){
                                          AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                           $location.path("/Login");
                                    }
                                if (!data.error) {
                                   if(data.data[0].continue == true){
                                       AgmsFactory.showToast(data.data[0].message, 5000);
                                       AgmsFactory.showToast(data.data[0].business_id,5000);
                                       $scope.user.phoneNumber = null;
                                       $('#userPhone').removeClass("active");
                                            $location.path("/Signup");
                                   }
                                   else{
                                       AgmsFactory.showToast(data.data[0].message, 5000);
                                       $scope.user.phoneNumber = null;
                                        $('#userPhone').removeClass("active");

                                   }

                                } else {
                                    $('#loader').hide();
                                    AgmsFactory.showToast(data.errorMessage, 9000);
                                    $scope.user.phoneNumber = null;
                                    $('#userPhone').removeClass("active");

                                }
                            })
                            .error(function (data) {
                                $('#loader').hide();
                                if(data.errorMessage=="Bad or expired token."){
                                    AgmsFactory.showToast(JSON.stringify(data.errorMessage));
                                    $location.path("/Login");
                                }
                                else{
                                 AgmsFactory.showToast(data, 4000);
                                     AgmsFactory.showToast(data.errorMessage, 4000);

                                }
                            });
                }
            }
            else{
                AgmsFactory.showToast(NoInterNet, 7000);
            }
      }

      $scope.validate = function(){
        var phone_number = $scope.user.phoneNumber;

        if(phone_number == "" || phone_number == undefined){
            $scope.showMobileNumber = true;
            $scope.MobileValidMess = sign_mob;
        }
       }

        $scope.mob_Click = function () {
            $scope.phonevalue = sign_mob;
            $scope.showMobileNumber = false;
        }
        $scope.alreadyregistered = function(){
            $location.path("/Login");
        }


      //init();
        //............for getting login credentials .............//

    } catch (e) {
        console.log(e);
    }
}]);
