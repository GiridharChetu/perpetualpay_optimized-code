'use strict';
appAGMS.controller('QRCodeCtrl', ['$scope', '$rootScope', '$location', '$route', function ($scope, $rootScope, $location, $route) {

    try {
        // check api response 
        $rootScope.message = "";
        var infolength = 0;
        $scope.qrCodeinfo = JSON.parse(localStorage["businessData"]);
        $scope.CommanSFID = $scope.qrCodeinfo.data.sfid;
        currentPage = "QRcodePage";


        //......... for getting QR code..........//
        var init = function () {

            for (infolength; infolength <= $scope.qrCodeinfo.data.relationships.length; infolength++) {
                if ($scope.qrCodeinfo.data.relationships[infolength].sfid == sessionStorage["SelectedSFID"]) {
                    $scope.businesssfid = $scope.qrCodeinfo.data.relationships[infolength].sfid;
                    break;
                }
            }
            $scope.option = {
                id: $scope.CommanSFID,
                business: $scope.businesssfid
            };
            jQuery('#output').qrcode(JSON.stringify($scope.option));
            $scope.getbusinessInfo();
        }
        //......... for getting QR code..........//

        //......... for using back button..........//
        $scope.backbtn = function () {
             $rootScope.StopInterval = "true";
             sessionStorage["SelectedSFID"] = $scope.businesssfid;
            $location.path("/AppLayout");
        }

        //............. show business data..............//
        $scope.getbusinessInfo = function () {
            $scope.business = JSON.parse(localStorage["businessData"]);
            for (infolength; infolength <= $scope.business.data.relationships.length; infolength++) {
                if ($scope.business.data.relationships[infolength].sfid == sessionStorage["SelectedSFID"]) {
                    $scope.businesslogo = $scope.business.data.relationships[infolength].business_logo__c;
                    $scope.displayname = $scope.business.data.relationships[infolength].display_name__c;
                    $scope.reward = $scope.business.data.relationships[infolength].rewards;
                    $scope.businesssfid = $scope.business.data.relationships[infolength].sfid;
                    break;
                }
            }
        }
        //............. show business data..............//

        init();
    } catch (e) {
        console.log(e);
    }

 }]);
