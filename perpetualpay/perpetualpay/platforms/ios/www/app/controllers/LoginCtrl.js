'use strict';
appAGMS.controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$route', 'AgmsFactory', function ($scope, $rootScope, $location, $route, AgmsFactory) {
    try {
      $scope.user = {
                "username": "",
                "PWD": ""
            };


          currentPage = "LoginPage";

          $scope.customPopup = false;

         // check api response
            $rootScope.message = "";

        $scope.modelChng = function () {
            $('#modalChangePassword').show();

        }
        $scope.modalAcc = function () {
            $('#modalAccount').show();
        }

        $scope.modalAccOrg = function () {
            $('#modalAccountOrange').show();
        }
        $scope.modelChngHide = function () {
            $('#modalChangePassword').hide();
        }
        $scope.modalAccHide = function () {
            $('#modalAccount').hide();
        }
        $scope.modalAccOrgHide = function () {
            $('#modalAccountOrange').hide();
        }
        $scope.AppLayout = function () {
            $location.path("/AppLayout");
        }
        //............for getting login credentials .............//
        $scope.authuser = function (user) {
        $scope.customPopup = false;
            var username = user.username;
            var PWD = user.password;
           var UserInformation = {"username":username,"PWD":PWD};

           localStorage["UserInfo"] = UserInformation;

            if (AgmsFactory.getDeviceConnectivity() != "none") {
                if (((username == "" || username == undefined) && (PWD == "" || PWD == undefined)) || (username == "" || username == undefined) || (PWD == "" || PWD == undefined)) {
                    $scope.isValidated($scope.user);
                } else {

                    $('#loader').show();
                    AgmsFactory
                        .authUser($scope.user)
                        .success(function (data) {
                             $('#loader').hide();
                            if (!data.error) {
                               token = data.token;
                               localStorage["Token"] = token;
                               Type = data.data[0].type;
                               phone = data.data[0].phone;
                                if(Type == "business"){

                                    $scope.customPopup = true;
                                    return false;
                                }

                                $location.path("/AppLayout");

                            } else {
                                $('#loader').hide();
                                AgmsFactory.showToast(data.errorMessage, 4000);
                            }
                        })
                        .error(function (data) {
                            $('#loader').hide();
                            AgmsFactory.showToast(data.errorMessage);
                        });
                }
            } else {
                AgmsFactory.showToast(NoInterNet, 7000);
            }

        }
        //............for getting login credentials .............//

        //........... Login field validation check...............//
        $scope.isValidated = function (user) {
            if (user != undefined) {
                var username = user.username;
                var PWD = user.password;
                if ((username == "" || username == undefined) && (PWD == "" || PWD == undefined)) {
                    $scope.showIDValidMess = true;
                    $scope.IDValidMess = validateUserMsg;
                    $scope.showPWDValidMess = true;
                    $scope.PWDValidMess = validatePwdMsg;
                    return false;
                } else if ((username == "" || username == undefined)) {
                    $scope.showIDValidMess = true;
                    $scope.IDValidMess = validateUserMsg;
                    return false;
                } else if ((PWD == "" || PWD == undefined)) {
                    $scope.showPWDValidMess = true;
                    $scope.PWDValidMess = validatePwdMsg;
                    return false;
                } else {
                    return false;
                }

            } else {
                $scope.showIDValidMess = true;
                $scope.IDValidMess = validateUserMsg;
                $scope.showPWDValidMess = true;
                $scope.PWDValidMess = validatePwdMsg;
                return false;
                return false;
            }

        }

        $scope.UID_Click = function () {
            $scope.showIDValidMess = false;
        }
        $scope.PWD_Click = function () {
            $scope.showPWDValidMess = false;
        }

       //........... Login field validation check...............//

        $scope.backbtn = function(user){
             $scope.customPopup = false;
             $scope.user.username = null;
             $scope.user.password = null;
             $("#removeUser").removeClass("active");
             $("#removePass").removeClass("active");

        }

         //........... CALL WHEN PRESS KEYBOARD ENTER KEY...............//
            $("#password").keypress(function (e) {
                var code = e.keyCode || e.which;
                if (code == 13) { //Enter keycode
                    $scope.authuser($scope.user);
                    document.activeElement.blur();
                }
            });
          //........... CALL WHEN PRESS KEYBOARD ENTER KEY...............//
          //................ GET MOBILE BROWSER HEIGHT...................//
            var windowHeight = $(window).height();
            $(function() {
                   $('.powered-by').css('margin-top', windowHeight);
            });
          //................ GET MOBILE BROWSER HEIGHT...................//

    } catch (e) {
        console.log(e);
    }
}]);
