'use strict';
var appAGMS = angular.module('app', ['ngRoute','ngTouch','ngAnimate']);
appAGMS.config(function ($routeProvider, $httpProvider ) {
    $routeProvider.
    when("/IndexWhite", {
        templateUrl: "views/index-white.html",
        controller: "IndexWhiteCtrl",
    }).
    when("/Signup", {
        templateUrl: "views/sign-up.html",
        controller: "SignupCtrl",
    }).
    when("/Login", {
        templateUrl: "views/login.html",
        controller: "LoginCtrl",
    }).
    when("/AppLayout", {
        templateUrl: "views/app-layout.html",
        controller: "AppLayoutCtrl",
    }).
    when("/QRCode", {
        templateUrl: "views/qr-code.html",
        controller: "QRCodeCtrl",
    }).
    when("/FriendInvite", {
        templateUrl: "views/friend-invite.html",
        controller: "FriendInviteCtrl",
    }).
    when("/Accounts", {
        templateUrl: "views/Accounts.html",
        controller: "AccountsCtrl",
    }).
    when("/IndexWhite", {
        templateUrl: "views/index-white.html",
        controller: "IndexWhiteCtrl"
    }).
    when("/facebooklist", {
            templateUrl: "views/Facebooklist.html",
            controller: "FacebooklistCtrl"
        }).
     when("/PhoneNumber", {
            templateUrl: "views/phone-number.html",
            controller: "PhoneNumberCtrl"
     }).
     when("/ChangePassword", {
             templateUrl: "views/ChangePassword.html",
             controller: "ChangePasswordCtrl"
     }).
      when("/BusinessInformation", {
             templateUrl: "views/BusinessInformation.html",
             controller: "BusinessInformationCtrl"
      }).
    otherwise({
        redirectTo: "/IndexWhite",
    });
}).run(function ($location, AgmsFactory, $rootScope) {

    if(localStorage["Token"] != "" && localStorage["Token"] != undefined && localStorage["Token"] != "undefined"){

        if(localStorage["businessData"] != "" && localStorage["businessData"] != undefined){
               if(JSON.parse(localStorage["businessData"]).error == true){
                   AgmsFactory.showToast(JSON.parse(localStorage["businessData"]).errorMessage,4000);
                   $location.path("/Login");
                   return;
               }
               else{
                   $location.path("/AppLayout");

               }
        }
        else{
            $location.path("/Login");
        }
    }
    else{
        $location.path("/Login");
    }
  });